#ifndef SCENEGAME_H
#define SCENEGAME_H

#include "Scene.h"
#include "Entity.h"
#include "Player.h"
#include "EnemyCaster.h"
#include "EnemyCannon.h"
#include "Tower.h"

//! SceneGame class
/*!
	Handles the Scene for the levels of the game.
*/
class SceneGame : public Scene
{
	public:
		//! Constructor of an empty SceneMenu.
		SceneGame();

		//! Destructor
		~SceneGame();

		//! Initializes the Scene.
		virtual void init();

		//! Loads the scene (reinitializes)
		virtual void load();

		//! Returns the ClassName of the object
		/*!
			\return ClassName (as a string)
		*/
		virtual std::string getClassName(){return "SceneGame";};

	protected:
		//! Updates the Scene
		void updateScene();

		//! Updates the Camera View
		void updateCamera();

		//! Draws the Scene
		void drawScene();
	
		//! Takes keyboard input and performs actions
		virtual void inputEvent();

		//! Loads map and objects onto the level
		void loadLevel(const char* level_name);

		//! Instantiates the objects for the level
		void loadObjects(const char* file_path);

		//! Check if the game is finished
		void checkState();

		void updateInputs();

		std::vector<Entity*> mpVectorEntities;
		Player*				 mpPlayer;

		C_Rectangle		mpCameraRect;
		unsigned long	mpTime;
};

#endif
