#include "singletons.h"

//Include our classes
#include "SceneGame.h"

#define byte char
#define Uint16 unsigned short int
#define Sint16 short int
#define Uint32 unsigned int
#define Sint32 int

SceneGame::SceneGame(): Scene(){ //Calls constructor in class Scene
}

SceneGame::~SceneGame(){
}

void SceneGame::init(){
	Scene::init(); //Calls to the init method in class Scene
	loadLevel("test");
	sMapManager->setTileset("tileset.png");
	mpCameraRect = C_Rectangle{ 0,0,SCREEN_WIDTH, SCREEN_HEIGHT };
	mpTime = 0;
}

void SceneGame::load(){
	Scene::load(); //Calls to the init method in class Scene
}

void SceneGame::updateScene(){
	mpTime += global_delta_time;
	if (mpTime + global_delta_time >= ULONG_MAX) { exit(0); }
	std::cout << mpTime << std::endl;
	inputEvent();
	updateInputs();
	for (int i = 0; i < mpVectorEntities.size(); i++) {
		if (!mpVectorEntities[i]->isInsideRectangle(mpCameraRect)) { //Si est� fuera de c�mara, pasamos
			if (mpVectorEntities[i]->isOfClass("Entity")) {
				mpVectorEntities[i]->update();
			}
			continue;
		}//Si no, seguimos haciendo el update tal cual
		mpVectorEntities[i]->update();
		//Mirar colisiones con otras entidades

		// Detectar la colision de los ataques
		if (C_RectangleCollision(mpPlayer->getAttackQ(), mpVectorEntities[i]->getRect()) && mpVectorEntities[i] != mpPlayer) {
			mpVectorEntities[i]->dealDamage(mpPlayer->getAttackDamage('q'));
		}
		if (C_RectangleCollision(mpPlayer->getRect(), mpVectorEntities[i]->getAttackQ()) && mpVectorEntities[i] != mpPlayer) {
			mpPlayer->dealDamage(mpVectorEntities[i]->getAttackDamage('q'));
		}


		checkState();
		mpVectorEntities[i]->checkRange(mpPlayer);
	}
	
	//Update Camera
	updateCamera();
}

void SceneGame::updateCamera() {//Camera follows Player Entity
	if (mpPlayer == NULL) { return; }
	
	if (mouse.x < 100) { mpCameraRect.x -= 4; }
	if (mouse.x > SCREEN_WIDTH - 100) { mpCameraRect.x += 4; }
	if (mouse.y < 100) { mpCameraRect.y -= 4; }
	if (mouse.y > SCREEN_HEIGHT - 100) { mpCameraRect.y += 4; }
	if (key_down[' ']) {
		mpCameraRect.x = (mpPlayer->getX() + mpPlayer->getW() / 2) - mpCameraRect.w / 2;
		mpCameraRect.y = (mpPlayer->getY() + mpPlayer->getH() / 2) - mpCameraRect.h / 2;
	}
	if (mpCameraRect.x < 0) { mpCameraRect.x = 0; }
	if (mpCameraRect.y < 0) { mpCameraRect.y = 0; }
	int mapWidth = (sMapManager->getMapWidth() - 1) * TILE_SIZE;
	int mapHeight = (sMapManager->getMapHeight() - 1) * TILE_SIZE;
	if (mpCameraRect.x + mpCameraRect.w > mapWidth) { mpCameraRect.x = mapWidth - mpCameraRect.w; }
	if (mpCameraRect.y + mpCameraRect.h > mapHeight) { mpCameraRect.y = mapHeight - mpCameraRect.h; }
}

void SceneGame::drawScene(){
	//Render map under objects
	sMapManager->renderMap(mpCameraRect, false);
	//Render Vector Entities
	int size = mpVectorEntities.size();
	for (int i = 0; i < mpVectorEntities.size(); i++) {
		if (!mpVectorEntities[i]->isInsideRectangle(mpCameraRect)) { //Si est� fuera de c�mara, pasamos
			if (mpVectorEntities[i] == mpPlayer) {
				mpPlayer->render();
			}
			continue;
		}//Si no, seguimos haciendo el render tal cual
		mpVectorEntities[i]->render(mpCameraRect.x, mpCameraRect.y); //Renderiza seg�n posici�n de la c�mara
		//Si se quiere renderizar en posici�n absoluta, render()
	}
	//Render map over objects
	sMapManager->renderMap(mpCameraRect, true);
}

//-------------------------------------------
//				   INPUT
//-------------------------------------------
void SceneGame::inputEvent(){
	if (key_released[8]) {//Backspace
		sDirector->goBack();
	}

	//CAMERA TEST
	/*
	if (key_released['j']) { mpCameraRect.x += 10; }
	if (key_released['l']) { mpCameraRect.x -= 10; }
	if (key_released['i']) { mpCameraRect.y -= 10; }
	if (key_released['k']) { mpCameraRect.y += 10; }
	*/
}

//--------------------------------------------
//				MAP AND OBJECTS
//--------------------------------------------
void SceneGame::loadLevel(const char* level_name) { //DO NOT TOUCH
	std::string map_path = "map/" + string(level_name);
	std::string obj_path = "map/" + string(level_name);
	map_path.append("_map.bin");
	obj_path.append("_obj.bin");
	sMapManager->loadMap(map_path.c_str());	//Load Level Tile Layers to MapManager
	loadObjects(obj_path.c_str());			//Load Level Objects to SceneGame (mpVectorEntities)
}

void SceneGame::loadObjects(const char* file_path) {
	FreeClear(mpVectorEntities);
	std::cout << "Loading Object file " << file_path << "." << std::endl;
	std::fstream file;
	file.open(file_path, std::ios::in | std::ios::binary);
	if (!file.is_open()) {
		std::cout << "ERROR opening file " << file_path << ". Is it there?" << std::endl;
		system("pause");
		exit(0);
		return;
	}

	Uint16 object_count = 0;	//Numero total de objetos
	file.read((byte*)&object_count, sizeof(Uint16));

	byte name_inc = 0;		//Nombre inclu�do?
	file.read((byte*)&name_inc, sizeof(byte));

	byte type_inc = 0;		//Tipo inclu�do?
	file.read((byte*)&name_inc, sizeof(byte));

	byte properties_inc = 0;//Propiedades inclu�das?
	file.read((byte*)&properties_inc, sizeof(byte));

	byte value_bits = 0;	//Value Bits
	file.read((byte*)&value_bits, sizeof(byte));

	for (int o = 0; o < object_count; o++) {//Empezamos a leer objetos
		byte shape; //Forma (0=tile, 1=rectangle, 2=ellipse, 3=polygon, 4=polyline). Solo se soporta Rectangle
		file.read((byte*)&shape, sizeof(byte));
		if (shape > 1) {
			std::cout << "Object not valid. Must be Rectangle" << std::endl;
			continue;
		}
		std::string name = "", type = "";
		if (name_inc > 0) {
			Uint16 size = 0;
			file.read((byte*)&size, sizeof(Uint16));
			for (Uint16 i = 0; i < size; i++) {
				char c;
				file.read((char*)&c, sizeof(char));
				name += c;
			}
		}
		if (type_inc > 0) {
			Uint16 size = 0;
			file.read((byte*)&size, sizeof(Uint16));
			for (Uint16 i = 0; i < size; i++) {
				char c;
				file.read((byte*)&c, sizeof(char));
				type += c;
			}
		}
		Uint16 x, y, w, h, rot, number;
		file.read((byte*)&x, sizeof(Uint16)); //X position
		file.read((byte*)&y, sizeof(Uint16)); //Y position

		file.read((byte*)&rot, sizeof(Uint16));//Rotation (not used)
		//file.read((byte*)&number, sizeof(Uint16));//Number (not used)
		file.read((byte*)&w, sizeof(Uint16)); //Width
		file.read((byte*)&h, sizeof(Uint16)); //Height

		std::vector<std::string> prop_names;
		std::vector<std::string> prop_values;
		Uint16 num_properties = 0;
		if (properties_inc > 0) {
			file.read((byte*)&num_properties, sizeof(Uint16));
			for (Uint16 p = 0; p < num_properties; p++) {
				std::string prop_name = "";
				std::string prop_value = "";
				Uint16 size = 0;
				file.read((byte*)&size, sizeof(Uint16));
				for (Uint16 i = 0; i < size; i++) {
					char c;
					file.read((byte*)&c, sizeof(char));
					prop_name += c;
				}

				byte prop_type = 0;
				file.read((byte*)&prop_type, sizeof(byte));
				
				if (prop_type == 0) {
					Uint16 value_num = 0;
					file.read((byte*)&value_num, sizeof(Uint16));
					prop_value = itos(value_num, 1);
				}else {
					size = 0;
					file.read((byte*)&size, sizeof(Uint16));
					for (Uint16 i = 0; i < size; i++) {
						char c;
						file.read((byte*)&c, sizeof(char));
						prop_value += c;
					}
				}
				prop_names.push_back(prop_name);
				prop_values.push_back(prop_value);
			}
		}
		std::cout << "Object found: " << name << " " << type << " at ["<<x<<","<<y<<"]  "<< "W:" << w << " H:"<<h<< std::endl;
		std::cout << ">> Object Properties: " << num_properties << std::endl;
		for (Uint16 i = 0; i < num_properties; i++) {
			std::cout << ">>>>> " << prop_names[i] << ": " << prop_values[i] << std::endl;
		}
		//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		//---TOCAR SOLO ENTRE ESTE COMENTARIO Y EL SIGUIENTE---
		// La info del objeto se encuentra en las variables name, x, y, w, h, prop_names[] y prop_values
		// Pod�is usarla para crear e insertar entidades en SceneGame, aqu� ten�is un ejemplo
		if (name == "Player") {
			Player* a_player = new Player();
			a_player->init(sResManager->getGraphicID("test.png"), x, y, w, h);
			a_player->setCamera(&mpCameraRect);
			mpPlayer = a_player;
			mpVectorEntities.push_back(a_player);
		}
		else if(name == "Cannon") {
			EnemyCannon* an_entity = new EnemyCannon();
			an_entity->init(sResManager->getGraphicID("test2.png"), x, y, w, h);
			an_entity->setCamera(&mpCameraRect);
			mpVectorEntities.push_back(an_entity);
			//Podemos leer los atributos de prop_names y prop_values
			//Solo recordad que est�n en String, por lo que hay que convertirlos
			//Sea a mano (usando if-else) o con funciones como stoi, etc.)
		}
		else if (name == "Caster") {
			EnemyCaster* an_entity = new EnemyCaster();
			an_entity->init(sResManager->getGraphicID("caster.png"), x, y, w, h);
			an_entity->setCamera(&mpCameraRect);
			mpVectorEntities.push_back(an_entity);
			//Podemos leer los atributos de prop_names y prop_values
			//Solo recordad que est�n en String, por lo que hay que convertirlos
			//Sea a mano (usando if-else) o con funciones como stoi, etc.)
		}
		else if (name == "Tower") {
			Tower* an_entity = new Tower();
			an_entity->init(sResManager->getGraphicID("tower.png"), x, y, w, h);
			an_entity->setCamera(&mpCameraRect);
			mpVectorEntities.push_back(an_entity);
			//Podemos leer los atributos de prop_names y prop_values
			//Solo recordad que est�n en String, por lo que hay que convertirlos
			//Sea a mano (usando if-else) o con funciones como stoi, etc.)
		}
		else {//Ir a�adiendo m�s casos
			//...
		}
		//-----------------------------------------------------
		//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	}
	file.close();
}

void SceneGame::updateInputs () {
	if (mouse.pressed[0]) {
		
	}
	if (mouse.pressed[1]) {

	}
	if (mouse.pressed[2]) {
		mpPlayer->activateAttack('n');
		int xx = mouse.x + mpCameraRect.x;
		int yy = mouse.y + mpCameraRect.y;
		C_Rectangle click;
		click.x = xx;
		click.y = yy;
		click.w = 6;
		click.h = 6;
		bool find = false;
		for (int i = 0; i < mpVectorEntities.size(); i++) {
			if (C_RectangleCollision(click, mpVectorEntities[i]->getRect()) && mpVectorEntities[i] != mpPlayer && mpVectorEntities[i]->getAlive()) {
				find = true;
				mpPlayer->setTarget(xx, yy, mpVectorEntities[i]);
			}
		}
		if (!find) {
			mpPlayer->setTarget(xx, yy, NULL);
		}
	}
}

void SceneGame::checkState() {
	bool alive = false;
	for (int i = 0; i < mpVectorEntities.size(); i++) {
		if (mpVectorEntities[i]->getAlive() && mpVectorEntities[i] != mpPlayer) { alive = true; }
	}

	if (!alive) { 
		sGameState->saveGame(mpTime);
		sDirector->changeScene(SceneDirector::GAME_OVER);
	}

}