#include "Attack.h"
#include "singletons.h"

Attack::Attack() : Entity() {
	mpRect.x = 0;
	mpRect.y = 0;
	mpRect.w = 8;
	mpRect.h = 8;

	mpStats.mpMoveSpeed = 300;

	mpGraphicRect.w = 8;
	mpGraphicRect.h = 8;

	mpAttacking = false;
	mpAlive = true;
	mpGraphicImg = sResManager->getGraphicID("bullet.png");
}

void Attack::init(int damage) {
	mpStats.mpAttackDamage = damage;
}

Attack::~Attack(){

}

void Attack::update(){
	Entity::update();
	if (mpAttacking) {
		if (mpTargetEntity == NULL) {
			mpAttacking = false;
			return;
		}
		int dist_x = (mpRect.x + mpRect.w / 2) - mpTargetX;
		int dist_y = (mpRect.y + mpRect.h / 2) - mpTargetY;
		int h = sqrt(pow(dist_x, 2) + pow(dist_y, 2));

		int speed_x = 0;
		int speed_y = 0;
		
		if (dist_x != 0 && dist_y != 0) {
			speed_x = mpStats.mpMoveSpeed * abs(dist_x) / h;
			speed_y = mpStats.mpMoveSpeed * abs(dist_y) / h;
		}


		if (mpRect.x + mpRect.w / 2 > mpTargetX) {
			mpRect.x -= speed_x * (int)global_delta_time / 1000;
			if (mpRect.x + mpRect.w / 2 < mpTargetX) {
				mpRect.x = mpTargetX - mpRect.w / 2;
			}
		} else if (mpRect.x + mpRect.w / 2 < mpTargetX) {
			mpRect.x += speed_x * (int)global_delta_time / 1000;
			if (mpRect.x + mpRect.w / 2 > mpTargetX) {
				mpRect.x = mpTargetX - mpRect.w / 2;
			}
		}

		if (mpRect.y + mpRect.w / 2 > mpTargetY) {
			mpRect.y -= speed_y * (int)global_delta_time / 1000;
			if (mpRect.y + mpRect.w / 2 < mpTargetY) {
				mpRect.y = mpTargetY - mpRect.w / 2;
			}
		}
		else if (mpRect.y + mpRect.w / 2 < mpTargetY) {
			mpRect.y += speed_y * (int)global_delta_time / 1000;
			if (mpRect.y + mpRect.w / 2 > mpTargetY) {
				mpRect.y = mpTargetY - mpRect.w / 2;
			}
		}

		// Hitting the enemy
		if (mpRect.x == mpTargetX - mpRect.w / 2 && mpRect.y == mpTargetY - mpRect.w / 2) {
			mpTargetEntity->dealDamage(mpStats.mpAttackDamage);
			mpAttacking = false;
		}

		mpTargetX = mpTargetEntity->getX() + mpTargetEntity->getW() / 2;
		mpTargetY = mpTargetEntity->getY() + mpTargetEntity->getW() / 2;
	}
	else {
		mpRect.x = 0;
		mpRect.y = 0;
	}
}
void Attack::render(int offX, int offY) {
	if (!mpAttacking) { return; }
	Entity::render(offX, offY);
}

bool Attack::isOfClass(std::string classType){
	if (classType == "Attack" ||
		classType == "Entity") {
		return true;
	}
	return false;
}

void Attack::setAttack(int x, int y, bool attack) {
	if (mpAttacking == attack) { return; }
	mpAttacking = attack;
	mpRect.x = x - mpRect.w / 2;
	mpRect.y = y - mpRect.h / 2;
	return;
}

void Attack::setTarget(int x, int y, Entity *entity) {
	if (mpAttacking) { return; }
	Entity::setTarget(x, y, entity);
}