#ifndef ENTITY_H
#define ENTITY_H

#include "ofMain.h"
#include "includes.h"
#include "Renderer.h"
#include "Utils.h"

typedef struct {
	int mpLife = 5;
	int mpCurrentLife = 5;

	int mpMoveSpeed = 0;

	int mpAttackSpeed = 0;
	int mpAttackSpeedTimer = 0;
	int mpAttackRange = 0;
	int mpAttackDamage = 0;

	int mpAttackOneDamage = 0;
	int mpAttackOneCooldown = 0;
	int mpAttackOneTimer = 0;
	bool mpAttackOneActivated = false;

} Stats;

class Entity
{
	public:
		Entity();
		~Entity();
		
		virtual void init();
		virtual void init(int x, int y);
		virtual void init(int graphic, int x, int y, int w, int h);
		virtual void render(int offX = 0, int offY = 0);
		virtual void update();

		//! Sets the target
		//! \param x Integer for the position
		//! \param y Integer for the position
		//! \param *Entity Pointer to the entity targeted
		virtual void setTarget(int x, int y, Entity *entity);

		//! Sets the X position for the instance
		//! \param x Integer for the position
		void setX(int x);

		//! Sets the Y position for the instance
		//! \param y Integer for the position
		void setY(int y);

		//! Sets X and Y position for the instance
		//! \param x Integer for the position in the X axes
		//! \param y Integer for the position in the Y axes
		void setXY(int x, int y);
		
		//! Sets the width for the instance
		//! \param w Integer for the width of the instance
		void setW(int w);

		//! Sets the height for the instance
		//! \param h Integer for the height of the instance
		void setH(int h);

		//! Sets the instance rectangle
		//! \param rect C_Rectangle
		void setRectangle(C_Rectangle rect);

		//! Sets the instance rectangle from variables
		//! \param x Integer for the position in the X axes
		//! \param y Integer for the position in the Y axes
		//! \param w Integer for the width of the instance
		//! \param h Integer for the height of the instance
		void setRectangle(int x, int y, int w, int h);

		//! Returns if the player's alive or not
		//! \return mpAlive (as a bool)
		bool getAlive() { return mpAlive; };

		//! Sets if player's alive or not
		//! \param alive Bool for alive (true = alive, false = dead)
		void setAlive(bool alive);

		//! Detects a collision between a rectangle and the instance
		//! \param a_rect C_Rectangle you want to check
		//! \return If colliding (as a bool)
		bool isInsideRectangle(C_Rectangle a_rect);

		//! Deals damage to the entity
		//! \param damage int Amount of damage you want to deal
		virtual void dealDamage(int damage);

		int getX(){return mpRect.x;};
		int getY(){return mpRect.y;};
		int getW(){return mpRect.w;};
		int getH(){return mpRect.h;};
		virtual C_Rectangle getAttackQ();
		virtual int getAttackDamage(char c);

		virtual void checkRange(Entity* objective);
		

		bool mpEnemy;
		C_Rectangle getRect() { return mpRect; };

		virtual bool isOfClass(std::string classType);
		std::string getClassName(){return "Entity";};

	protected:
		virtual void updateControls();
		virtual void setCamera(C_Rectangle *cam);
		bool checkCollisionWithMap(int x, int y);

		void move();

		virtual void updateGraphic();

		C_Rectangle 	mpRect;	//Collision and Position
		C_Rectangle 	mpGraphicRect;
		int				mpGraphicImg;

		int				mFrame;
		int				mMaxFrame;
		int				mCurrentFrameTime;
		int				mMaxFrameTime;
	
		bool 			mpAlive;

		int				mpDirection;
		bool			mpMoving;

		int				mpXtoGo;
		int				mpYtoGo;

		int				mpInitialX;
		int				mpInitialY;
		
		bool			mpTileBasedMovement;

		int				mpTargetX;
		int				mpTargetY;
		Entity*			mpTargetEntity;

		Stats			mpStats;
		std::vector <Entity*> *mpEntities;
};

#endif
