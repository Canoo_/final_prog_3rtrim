//Include our classes
#include "EnemyCannon.h"
#include "singletons.h"


EnemyCannon::EnemyCannon() : Entity(){
	mpStats.mpMoveSpeed = 300;
	mpStats.mpAttackRange = 150;
	mpViewRange = 50;
	mpStats.mpMoveSpeed = 50;
	mpStats.mpAttackOneDamage = 1;
	mpStats.mpAttackOneCooldown = 5000;
	mpStats.mpAttackOneTimer = 5000;

	mpAttackQ = new AttackQ();
	mpAttackQ->init(mpStats.mpAttackOneDamage);
}

EnemyCannon::~EnemyCannon(){
}

void EnemyCannon::init(){
	Entity::init();
}
void EnemyCannon::init(int x, int y){
	Entity::init(x, y);
}
void EnemyCannon::init(int graphic, int x, int y, int w, int h){
	Entity::init(graphic, x, y, w, h);
	mpTargetX = x;
	mpTargetY = y;
}

void EnemyCannon::render(int offX, int offY) {
	Entity::render(offX, offY);
	mpAttackQ->render(offX, offY);
}

void EnemyCannon::update() {
	Entity::update();
	mpAttackQ->update();
}

void EnemyCannon::updateControls() {	
	mpDirection = NONE;
	mpStats.mpAttackOneTimer += global_delta_time;

	if (mpRect.x + mpRect.w / 2 + 5 > mpTargetX && mpRect.x + mpRect.w / 2 - 5 < mpTargetX) {
		mpRect.x = mpTargetX - mpRect.w / 2;
	}
	if (mpRect.y + mpRect.h / 2 + 5 > mpTargetY && mpRect.y + mpRect.h / 2 - 5 < mpTargetY) {
		mpRect.y = mpTargetY - mpRect.h / 2;
	}


	if (mpRect.x + mpRect.w / 2 < mpTargetX && mpRect.y + mpRect.h / 2 > mpTargetY) {		// UP_RIGHT
		mpDirection = UP_RIGHT;
	}
	else if (mpRect.y + mpRect.h / 2 > mpTargetY && mpRect.x + mpRect.w / 2 > mpTargetX) {	// UP_LEFT
		mpDirection = UP_LEFT;
	}
	else if (mpRect.y + mpRect.h / 2 < mpTargetY && mpRect.x + mpRect.w / 2 < mpTargetX) {	// DOWN_RIGHT	
		mpDirection = DOWN_RIGHT;
	}
	else if (mpRect.y + mpRect.h / 2 < mpTargetY && mpRect.x + mpRect.w / 2 > mpTargetX) {	// DOWN_LEFT
		mpDirection = DOWN_LEFT;
	}
	else if (mpRect.x + mpRect.w/2 < mpTargetX) {
		mpDirection = RIGHT;
	}
	else if (mpRect.x + mpRect.w / 2 > mpTargetX) {
		mpDirection = LEFT;
	}
	else if (mpRect.y + mpRect.h / 2 > mpTargetY) {
		mpDirection = UP;
	}
	else if (mpRect.y + mpRect.h / 2 < mpTargetY) {
		mpDirection = DOWN;
	}


	if (mpTargetEntity != NULL) {
		mpTargetX = mpTargetEntity->getX() + mpTargetEntity->getW() / 2;
		mpTargetY = mpTargetEntity->getY() + mpTargetEntity->getW() / 2;
		int	px = mpRect.x + mpRect.w / 2;
		int	py = mpRect.y + mpRect.h / 2;
		int xx = px - mpTargetX;
		int yy = py - mpTargetY;
		if (sqrt(xx*xx + yy*yy) < mpStats.mpAttackRange) {
			if (mpStats.mpAttackOneTimer > mpStats.mpAttackOneCooldown) {
				mpAttackQ->setTarget(mpTargetX + mpCamera->x, mpTargetY + mpCamera->y , NULL);
				mpAttackQ->setAttack(mpRect.x + mpRect.w / 2, mpRect.y + mpRect.h / 2);
				mpStats.mpAttackOneTimer = 0;
			}
		}
	}
	

	return;
}

void EnemyCannon::setCamera(C_Rectangle *cam) {
	mpCamera = cam;
	return;
}

bool EnemyCannon::isOfClass(std::string classType){
	if(classType == "EnemyCannon" || 
		classType == "Entity"){
		return true;
	}
	return false;
}



void EnemyCannon::checkRange(Entity* objective) {
	int px = mpRect.x + mpRect.w / 2;
	int px2 = objective->getX() + objective->getW() / 2;
	int dx = abs(px - px2);

	int py = mpRect.y + mpRect.h / 2;
	int py2 = objective->getY() + objective->getH() / 2;
	int dy = abs(py - py2);


	if (dy <= mpViewRange) {
		mpTargetEntity = objective;
		mpTargetX = px2;
		mpTargetY = py2;
	}
	return;
}



C_Rectangle EnemyCannon::getAttackQ() {
	return mpAttackQ->getRect();
}

int EnemyCannon::getAttackDamage(char c) {
	mpAttackQ->setXY(-32, -32);
	return mpStats.mpAttackOneDamage;
}