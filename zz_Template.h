#ifndef TEMPLATE_H
#define TEMPLATE_H

#include "includes.h"

class Template
{
	public:
		Template();
		~Template();

		void update();
		void render();

		bool isOfClass(std::string classType);
		std::string getClassName(){return "Template";};

	protected:
	
	private:
	
};

#endif
