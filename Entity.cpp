//Include our classes
#include "Entity.h"
#include "singletons.h"


Entity::Entity(){
	mpAlive = true;
	setRectangle(0, 0, TILE_SIZE, TILE_SIZE);
	
	mpGraphicRect.x = 0;
	mpGraphicRect.y = 0;
	mpGraphicRect.w = 0;
	mpGraphicRect.h = 0;

	mpGraphicImg = -1;
	
	mFrame = 0;
	mMaxFrame = 0;
	mCurrentFrameTime = 0;
	mMaxFrameTime = 150;

	mpDirection = NONE;
	mpMoving = false;

	mpXtoGo = mpRect.x;
	mpYtoGo = mpRect.y;
	
	mpTileBasedMovement = false;


	mpRect.x = 0;
	mpRect.y = 0;
	mpTargetEntity = NULL;
	mpTargetX = 0;
	mpTargetY = 0;
	mpEnemy = false;

	mpStats.mpLife = 1;
	mpStats.mpCurrentLife = 1;
	mpStats.mpAttackDamage = 1;
	
}

Entity::~Entity(){
}

void Entity::init(){
	mpAlive = true;
	mpTargetX = mpRect.x + mpRect.w / 2;
	mpTargetY = mpRect.x + mpRect.w / 2;
}

void Entity::init(int x, int y) {
	init();
	mpInitialX = x;
	mpInitialY = y;

	mpRect.x = mpInitialX;
	mpRect.y = mpInitialY;
	
	mpXtoGo = mpRect.x;
	mpYtoGo = mpRect.y;
}

void Entity::init(int graphic, int x, int y, int w, int h) {
	init(x, y);
	mpRect.w = w;
	mpRect.h = h;
	mpGraphicImg = graphic;
	mpGraphicRect = C_Rectangle{0,0,(unsigned int)w,(unsigned int)h};
}

void Entity::update(){
	if (!mpAlive) { return; }
	if (mpTargetEntity != NULL) {
		if (!mpTargetEntity->getAlive()) {
			mpTargetEntity = NULL;
			mpTargetX = mpRect.x + mpRect.w / 2;
			mpTargetY = mpRect.y + mpRect.h / 2;
		}
	}
	updateControls();
	move();
	updateGraphic();
}

void Entity::updateGraphic() {
	mCurrentFrameTime += global_delta_time;
	if (mCurrentFrameTime > mMaxFrameTime) {
		mCurrentFrameTime = 0;
		mFrame++;
		if (mFrame >= mMaxFrame) {
			mFrame = 0;
		}
	}
	mpGraphicRect.x = mFrame*mpGraphicRect.w;
}

void Entity::render(int offX, int offY){
	if (!mpAlive || mpGraphicImg < 0) { return; }
	imgRender(mpGraphicImg, mpRect.x - offX, mpRect.y - offY, mpGraphicRect);
}

void Entity::updateControls() {
	
	return;
}

void Entity::move() {
	int xx = mpRect.x;
	int yy = mpRect.y;
	switch (mpDirection) {
		case UP_RIGHT:
			yy = yy - (mpStats.mpMoveSpeed/1.42)*global_delta_time / 1000;
			if (checkCollisionWithMap(xx + 1, yy) ||
				checkCollisionWithMap(xx + mpRect.w / 2, yy) ||
				checkCollisionWithMap(xx + mpRect.w - 1, yy))
			{
				int tile = yy / TILE_SIZE + 1;
				int dif = tile * TILE_SIZE - yy;
				yy += dif;
			}
			xx = xx + (mpStats.mpMoveSpeed/1.42)*global_delta_time / 1000;
			if (checkCollisionWithMap(xx + mpRect.w, yy + 1) ||
				checkCollisionWithMap(xx + mpRect.w, yy + mpRect.h / 2) ||
				checkCollisionWithMap(xx + mpRect.w, yy + mpRect.h - 1))
			{
				int tile = xx / TILE_SIZE + 1;
				int dif = tile * TILE_SIZE - (xx + mpRect.w);
				xx += dif;
			}
			break;
		case UP_LEFT:
			yy = yy - (mpStats.mpMoveSpeed/1.42)*global_delta_time / 1000;
			if (checkCollisionWithMap(xx + 1, yy) ||
				checkCollisionWithMap(xx + mpRect.w / 2, yy) ||
				checkCollisionWithMap(xx + mpRect.w - 1, yy))
			{
				int tile = yy / TILE_SIZE + 1;
				int dif = tile * TILE_SIZE - yy;
				yy += dif;
			}
			xx = xx - (mpStats.mpMoveSpeed/1.42)*global_delta_time / 1000;
			if (checkCollisionWithMap(xx, yy + 1) ||
				checkCollisionWithMap(xx, yy + mpRect.h / 2) ||
				checkCollisionWithMap(xx, yy + mpRect.h - 1))
			{
				int tile = xx / TILE_SIZE + 1;
				int dif = tile * TILE_SIZE - xx;
				xx += dif;
			}
			break;
		case DOWN_RIGHT:
			yy = yy + (mpStats.mpMoveSpeed/1.42)*global_delta_time / 1000;
			if (checkCollisionWithMap(xx + 1, yy + mpRect.h) ||
				checkCollisionWithMap(xx + mpRect.w / 2, yy + mpRect.h) ||
				checkCollisionWithMap(xx + mpRect.w - 1, yy + mpRect.h))
			{
				int tile = yy / TILE_SIZE + 1;
				int dif = tile * TILE_SIZE - (yy + mpRect.h);
				yy += dif;
			}
			xx = xx + (mpStats.mpMoveSpeed/1.42)*global_delta_time / 1000;
			if (checkCollisionWithMap(xx + mpRect.w, yy + 1) ||
				checkCollisionWithMap(xx + mpRect.w, yy + mpRect.h / 2) ||
				checkCollisionWithMap(xx + mpRect.w, yy + mpRect.h - 1))
			{
				int tile = xx / TILE_SIZE + 1;
				int dif = tile * TILE_SIZE - (xx + mpRect.w);
				xx += dif;
			}
			break;
		case DOWN_LEFT:
			yy = yy + (mpStats.mpMoveSpeed/1.42)*global_delta_time / 1000;
			if (checkCollisionWithMap(xx + 1, yy + mpRect.h) ||
				checkCollisionWithMap(xx + mpRect.w / 2, yy + mpRect.h) ||
				checkCollisionWithMap(xx + mpRect.w - 1, yy + mpRect.h))
			{
				int tile = yy / TILE_SIZE + 1;
				int dif = tile * TILE_SIZE - (yy + mpRect.h);
				yy += dif;
			}
			xx = xx - (mpStats.mpMoveSpeed/1.42)*global_delta_time / 1000;
			if (checkCollisionWithMap(xx, yy + 1) ||
				checkCollisionWithMap(xx, yy + mpRect.h / 2) ||
				checkCollisionWithMap(xx, yy + mpRect.h - 1))
			{
				int tile = xx / TILE_SIZE + 1;
				int dif = tile * TILE_SIZE - xx;
				xx += dif;
			}
			break;
		case UP:
			yy = yy - mpStats.mpMoveSpeed*global_delta_time / 1000;
			if (checkCollisionWithMap(xx + 1, yy) ||
				checkCollisionWithMap(xx + mpRect.w / 2, yy) || 
				checkCollisionWithMap(xx + mpRect.w - 1, yy))
			{
				int tile = yy / TILE_SIZE + 1;
				int dif = tile * TILE_SIZE - yy;
				yy += dif;
			}
			break;
		case DOWN:
			yy = yy + mpStats.mpMoveSpeed*global_delta_time / 1000;
			if (checkCollisionWithMap(xx + 1, yy + mpRect.h) ||
				checkCollisionWithMap(xx + mpRect.w / 2, yy + mpRect.h) || 
				checkCollisionWithMap(xx + mpRect.w - 1, yy + mpRect.h))
			{
				int tile = yy / TILE_SIZE + 1;
				int dif = tile * TILE_SIZE - (yy+mpRect.h );
				yy += dif;
			}
			break;
		case LEFT:
			xx = xx - mpStats.mpMoveSpeed*global_delta_time / 1000;
			if (checkCollisionWithMap(xx, yy + 1) ||
				checkCollisionWithMap(xx, yy + mpRect.h / 2) || 
				checkCollisionWithMap(xx, yy + mpRect.h - 1))
			{
				int tile = xx / TILE_SIZE + 1;
				int dif = tile * TILE_SIZE - xx;
				xx += dif;
			}
			break;
		case RIGHT:
			xx = xx + mpStats.mpMoveSpeed*global_delta_time / 1000;
			if (checkCollisionWithMap(xx + mpRect.w, yy + 1) ||
				checkCollisionWithMap(xx + mpRect.w, yy + mpRect.h / 2) ||
				checkCollisionWithMap(xx + mpRect.w, yy + mpRect.h - 1))
			{
				int tile = xx / TILE_SIZE + 1;
				int dif = tile * TILE_SIZE - (xx + mpRect.w );
				xx += dif;
			}
			break;
		default: 
			break;
	}
	mpRect.x = xx;
	mpRect.y = yy;
	return;
}

bool Entity::checkCollisionWithMap(int x, int y) {
	return sMapManager->getCollision(x/TILE_SIZE, y/TILE_SIZE);
}

void Entity::setX(int x) {
	mpRect.x = x;
	return;
}

void Entity::setY(int y) {
	mpRect.y = y;
	return;
}

void Entity::setXY(int x, int y) {
	setX(x);
	setY(y);
	return;
}

void Entity::setW(int w) {
	mpRect.w = w;
	return;
}

void Entity::setH(int h) {
	mpRect.h = h;
	return;
}

void Entity::setRectangle(C_Rectangle rect) {
	mpRect = rect;
	return;
}

void Entity::setRectangle(int x, int y, int w, int h) {
	C_Rectangle a_rect = { x, y, w, h };
	setRectangle(a_rect);
	return;
}

bool Entity::isInsideRectangle(C_Rectangle a_rect) {
	if (C_RectangleTouch(mpRect, a_rect)) {
		return true;
	}
	return false;
}

void Entity::setAlive(bool alive) {
	mpAlive = alive;
	return;
}


bool Entity::isOfClass(std::string classType){
	if(classType == "Entity"){
		return true;
	}
	return false;
}

void Entity::setTarget(int x, int y, Entity *entity) {
	mpTargetX = x;
	mpTargetY = y;
	mpTargetEntity = entity;
}

void Entity::dealDamage(int damage) {
	mpStats.mpCurrentLife -= damage;
	if (mpStats.mpCurrentLife <= 0) {
		setAlive(false);
	}
}

void Entity::setCamera(C_Rectangle *cam) {
	return;
}

C_Rectangle Entity::getAttackQ() {
	C_Rectangle r;
	r.x = 0;
	r.y = 0;
	r.w = 0;
	r.h = 0;
	return r;
}

void Entity::checkRange(Entity* objective) { return; }

int Entity::getAttackDamage(char c) { return 0; }