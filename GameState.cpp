//Include our classes
#include "GameState.h"


GameState* GameState::pInstance = NULL;

GameState::GameState(){
	
}

GameState::~GameState(){
}

void GameState::saveGame(unsigned long time) {
	
	file.open("data.txt", std::ios::in | std::ios::binary);
	if (!file.is_open()) { exit(0); }
	file.read((char*) &pts, sizeof(int));
	file.close();


	if (time < pts) {
		pts = time;
	}

	file.open("data.txt", std::ios::out | std::ios::binary | std::ios::trunc);
	if (!file.is_open()) { exit(0); }
	file.write((char*)&pts, sizeof(unsigned long));
	file.close();

	return;
}

GameState* GameState::getInstance() {
	//Si se ha inicializado, devuelve la instancia, si no
	//inicializa y la devuelve.
	if (!pInstance) {
		pInstance = new GameState();
	}
	return pInstance;
}


bool GameState::isOfClass(std::string classType){
	if(classType == "GameState"){
		return true;
	}
	return false;
}

