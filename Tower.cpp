//Include our classes
#include "Tower.h"
#include "singletons.h"


Tower::Tower() : Entity(){
	mpStats.mpMoveSpeed = 300;
	mpStats.mpAttackRange = 150;
	mpStats.mpAttackDamage = 1;
	mpStats.mpAttackSpeed = 1000;
	mpStats.mpAttackSpeedTimer = 1000;
	mpAttacks.resize(5);
	for (int i = 0; i < 5; i++) {
		mpAttacks[i] = new Attack();
		mpAttacks[i]->init(mpStats.mpAttackDamage);
	}
	mpAttackIndex = 0;
	mpViewRange = 50;
	mpStats.mpMoveSpeed = 50;
}

Tower::~Tower(){
}

void Tower::init(){
	Entity::init();
}
void Tower::init(int x, int y){
	Entity::init(x, y);
}
void Tower::init(int graphic, int x, int y, int w, int h){
	Entity::init(graphic, x, y, w, h);
	mpTargetX = x;
	mpTargetY = y;
}

void Tower::render(int offX, int offY) {
	Entity::render(offX, offY);
	for (int i = 0; i < 5; i++) {
		mpAttacks[i]->render(offX, offY);
	}
	ofDrawRectangle(10, 10, mpStats.mpAttackOneTimer / 10, 10);
}

void Tower::update() {
	if (!mpAlive) { return; }
	if (mpTargetEntity != NULL) {
		if (!mpTargetEntity->getAlive()) {
			mpTargetEntity = NULL;
			mpTargetX = mpRect.x + mpRect.w / 2;
			mpTargetY = mpRect.y + mpRect.h / 2;
		}
	}
	updateControls();
	updateGraphic();
	
	for (int i = 0; i < 5; i++) {
		mpAttacks[i]->update();
	}
}

void Tower::updateControls() {	
	mpDirection = NONE;
	mpStats.mpAttackSpeedTimer += global_delta_time;
	

	if (mpRect.x + mpRect.w / 2 + 5 > mpTargetX && mpRect.x + mpRect.w / 2 - 5 < mpTargetX) {
		mpRect.x = mpTargetX - mpRect.w / 2;
	}
	if (mpRect.y + mpRect.h / 2 + 5 > mpTargetY && mpRect.y + mpRect.h / 2 - 5 < mpTargetY) {
		mpRect.y = mpTargetY - mpRect.h / 2;
	}


	if (mpRect.x + mpRect.w / 2 < mpTargetX && mpRect.y + mpRect.h / 2 > mpTargetY) {		// UP_RIGHT
		mpDirection = UP_RIGHT;
	}
	else if (mpRect.y + mpRect.h / 2 > mpTargetY && mpRect.x + mpRect.w / 2 > mpTargetX) {	// UP_LEFT
		mpDirection = UP_LEFT;
	}
	else if (mpRect.y + mpRect.h / 2 < mpTargetY && mpRect.x + mpRect.w / 2 < mpTargetX) {	// DOWN_RIGHT	
		mpDirection = DOWN_RIGHT;
	}
	else if (mpRect.y + mpRect.h / 2 < mpTargetY && mpRect.x + mpRect.w / 2 > mpTargetX) {	// DOWN_LEFT
		mpDirection = DOWN_LEFT;
	}
	else if (mpRect.x + mpRect.w/2 < mpTargetX) {
		mpDirection = RIGHT;
	}
	else if (mpRect.x + mpRect.w / 2 > mpTargetX) {
		mpDirection = LEFT;
	}
	else if (mpRect.y + mpRect.h / 2 > mpTargetY) {
		mpDirection = UP;
	}
	else if (mpRect.y + mpRect.h / 2 < mpTargetY) {
		mpDirection = DOWN;
	}


	if (mpTargetEntity != NULL) {
		mpTargetX = mpTargetEntity->getX() + mpTargetEntity->getW() / 2;
		mpTargetY = mpTargetEntity->getY() + mpTargetEntity->getW() / 2;
		int	px = mpRect.x + mpRect.w / 2;
		int	py = mpRect.y + mpRect.h / 2;
		int xx = abs(px - mpTargetX);
		int yy = abs(py - mpTargetY);
		if (sqrt(xx*xx + yy*yy) < mpStats.mpAttackRange) {
			mpDirection = NONE;
			if (mpStats.mpAttackSpeedTimer >= mpStats.mpAttackSpeed) {
				mpStats.mpAttackSpeedTimer = 0;
				mpAttacks[mpAttackIndex]->setTarget(mpTargetX, mpTargetY, mpTargetEntity);
				mpAttacks[mpAttackIndex]->setAttack(mpRect.x + mpRect.w / 2, mpRect.y + mpRect.h / 2, true);
				mpAttackIndex++;
				if (mpAttackIndex >= 5) {
					mpAttackIndex = 0;
				}
			}
		}
	}
	

	return;
}

void Tower::setCamera(C_Rectangle *cam) {
	mpCamera = cam;
	return;
}

bool Tower::isOfClass(std::string classType){
	if(classType == "Tower" || 
		classType == "Entity"){
		return true;
	}
	return false;
}



void Tower::checkRange(Entity* objective) {
	int px = mpRect.x + mpRect.w / 2;
	int px2 = objective->getX() + objective->getW() / 2;
	int dx = abs(px - px2);

	int py = mpRect.y + mpRect.h / 2;
	int py2 = objective->getY() + objective->getH() / 2;
	int dy = abs(py - py2);


	if (dy <= mpViewRange) {
		mpTargetEntity = objective;
		mpTargetX = px2;
		mpTargetY = py2;
	}
	return;
}

void Tower::move() { return; }