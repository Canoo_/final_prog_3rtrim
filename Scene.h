#ifndef SCENE_H
#define SCENE_H

#include "includes.h"
#include "Renderer.h"
#include "Utils.h"
#include "ofMain.h"

//! Scene class
/*!
	Handles the Scenes for all the game.
*/
class Scene
{
	public:
		//! Constructor of an empty Scene.
		Scene();

		//! Destructor
		~Scene();

		//! Initializes the Scene.
		virtual void init();

		//! Loads the scene (reinitializes)
		virtual void load();

		//! Handles the drawing of the scene
		void onDraw();

		//! Handles the updating of the scene
		void onUpdate();

		void setReloadOnReturn(bool reload = true){mReload = reload;};
		void setLoaded(bool loaded = true)	{mLoaded = loaded;};
		bool isLoaded()						{return mLoaded;};

		//! Returns the ClassName of the object
		/*!
			\return ClassName (as a string)
		*/
		virtual std::string getClassName(){return "Scene";};

	protected:
		//! Updates the Scene
		virtual void updateScene() {};

		//! Draws the Scene
		virtual void drawScene() {};

		//! Takes keyboard input and performs actions
		virtual void inputEvent();

		bool		mLoaded;			/*!<  If the Scene has been loaded*/
		bool		mReload;			/*!<  Reload when returning to scene*/
};

#endif
