#ifndef PLAYER_H
#define PLAYER_H

#include "Entity.h"
#include "Attack.h"
#include "AttackQ.h"

class Player : public Entity
{
	public:
		Player();
		~Player();
		
		virtual void init();
		virtual void init(int x, int y);
		virtual void init(int graphic, int x, int y, int w, int h);
		void dealDamage(int damage);

		virtual void render(int offX = 0, int offY = 0);
		virtual void update();
		void activateAttack(char attack);
		C_Rectangle getAttackQ();
		int getAttackDamage(char c);

		void updateControls();
		
		bool isOfClass(std::string classType);
		std::string getClassName(){return "Player";};

		//! Stores a pointer to the camera
		//! \param cam C_Rectangle of the camera
		void setCamera(C_Rectangle *cam);

	protected:


	private:
		C_Rectangle				*mpCamera;
		std::vector <Attack*>	mpAttacks;
		AttackQ*				mpAttackQ;
		int						mpAttackIndex;
		int						mpInfoGraphic;
		int						mpLifeID;
		int						mpManaID;


		void checkKeys();
};

#endif
