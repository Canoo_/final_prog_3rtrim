#ifndef GAMESTATE_H
#define GAMESTATE_H

#include "includes.h"
#include <fstream>

class GameState
{
	public:
		GameState();
		~GameState();

		void saveGame(unsigned long time);
		static GameState* getInstance();
		bool isOfClass(std::string classType);
		std::string getClassName(){return "GameState";};

	protected:
		static GameState*		pInstance;
	
	private:
		std::fstream			file;
		unsigned long			pts;
};

#endif
