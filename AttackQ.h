#ifndef ATTACKQ_H
#define ATTACKQ_H

#include "Entity.h"

class AttackQ : public Entity
{
	public:
		AttackQ();
		void init(int damage);
		~AttackQ();

		void update();
		void render(int offX = 0, int offY = 0);
		void setAttack(int x, int y);

		bool isOfClass(std::string classType);
		std::string getClassName(){return "AttackQ";};
		void setTarget(int x, int y, Entity *entity);

	protected:
	
	private:
		bool mpAttacking;
		int mpRange;
		int speed_x;
		int speed_y;
	
};

#endif
