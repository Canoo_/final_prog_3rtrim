#ifndef MAPMANAGER_H
#define MAPMANAGER_H

#include "includes.h"


class MapManager
{
	public:
		MapManager();
		~MapManager();

		void loadMap(const char* file_path);
		void renderMap(C_Rectangle cam, bool over = false);

		void setTileset(const char* path);

		//!Sets which layer will be the objects displayed on
		void setObjectLayer(unsigned int layer) { mpObjectLayer = layer; };

		//!Gets if the given tile collides or not
		bool getCollision(unsigned int x, unsigned int y);


		int getMapWidth() { return mpMapWidth; };
		int getMapHeight(){ return mpMapHeight;};
		
		std::string getClassName(){return "MapManager";};

		//! Gets Singleton instance
		/*!
			\return Instance of MapManager (Singleton).
		*/
		static MapManager* getInstance();

	protected:
		static MapManager*		pInstance;		/*!<  Singleton instance*/

		//Attributes
		int						mpTilesetID;
		C_Rectangle				mpTilesetRect;
		int						mpTilesetW;
		int						mpTilesetH;
		unsigned int			mpMapWidth;
		unsigned int			mpMapHeight;
		bool					mpMapLoaded;

		unsigned int			mpLayers;
		unsigned int			mpObjectLayer;

		std::vector< std::vector<bool> >				 mpCollisionLayer;
		std::vector< std::vector < std::vector <int> > > mpMapLayers;
};

#endif
