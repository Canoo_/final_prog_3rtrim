#include "ofApp.h"
#include "singletons.h"
#include "includes.h"

const clock_t begin_time = clock();
clock_t old_time;
clock_t new_time;
unsigned int global_delta_time = 0;

bool key_down[255];
bool key_pressed[255];
bool key_released[255];

MouseManager mouse;

//--------------------------------------------------------------
void ofApp::setup(){
	instanceSingletons();
	//Init scenes
	sDirector->init();

	//Control
	for (int i = 0; i < 255; i++) {
		key_down[i] = false;
		key_pressed[i] = false;
		key_released[i] = false;
	}
	for (int i = 0; i < 3; i++) {
		mouse.down[i] = false;
		mouse.pressed[i] = false;
		mouse.released[i] = false;
	}
	//Time
	ofSetFrameRate(60);
	old_time = begin_time;
	new_time = begin_time;
}

//--------------------------------------------------------------
void ofApp::update(){
	//Delta time update (HAS TO BE DONE ALWAYS, ERRORS COULD HAPPEN OTHERWISE)
	old_time = new_time;
	new_time = clock() - begin_time;
	global_delta_time = int(new_time - old_time);
	//---------------------------------------------------

	//Check if Scene is loaded, if not, load it
	if (!sDirector->getCurrentScene()->isLoaded()) {
		sDirector->getCurrentScene()->load();
	}
	//Update Scene
	sDirector->getCurrentScene()->onUpdate();

	//End update
	for (int i = 0; i < 255; i++) { //Control for pressed and released events
		key_pressed[i] = false;
		key_released[i] = false;
	}
	for (int i = 0; i < 3; i++) {
		mouse.pressed[i] = false;
		mouse.released[i] = false;
	}
	mouse.x = ofGetMouseX();
	mouse.y = ofGetMouseY();
}

//--------------------------------------------------------------
void ofApp::draw(){
	ofClear(0, 0, 0);
	sDirector->getCurrentScene()->onDraw();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
	if (key >= 255 || key < 0) { return; }
	if (!key_down[key]) { //Only set pressed to true if the key is not being hold down 
						  //(pressed for first time)
		key_pressed[key] = true;
	}
	key_down[key] = true;
	key_released[key] = false;
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){
	if (key >= 255 || key < 0) { return; }
	key_down[key] = false;
	key_pressed[key] = false;
	key_released[key] = true;
}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){
	if (!mouse.down[button]) {
		mouse.pressed[button] = true;
	}
	mouse.down[button] = true;
	mouse.released[button] = false;
}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){
	mouse.down[button] = false;
	mouse.pressed[button] = false;
	mouse.released[button] = true;
}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){
	ofSetWindowShape(SCREEN_WIDTH, SCREEN_HEIGHT);
}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}

