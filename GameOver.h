#ifndef GameOver_H
#define GameOver_H

#include "Scene.h"

//! GameOver class
/*!
	Handles the Scene for the main menu of the game.
*/
class GameOver : public Scene
{
	public:
		//! Constructor of an empty GameOver.
		GameOver();

		//! Destructor
		~GameOver();

		//! Initializes the Scene.
		virtual void init();

		//! Loads the scene (reinitializes)
		virtual void load();

		//! Returns the ClassName of the object
		/*!
			\return ClassName (as a string)
		*/
		virtual std::string getClassName(){return "GameOver";};

	protected:
		//! Updates the Scene
		void updateScene();

		//! Draws the Scene
		void drawScene();
	
		//! Takes keyboard input and performs actions
		virtual void inputEvent();

	private:
		int			mpGraphicID;
		C_Rectangle	mpGraphicRect;
		int			mpTime;
};

#endif
