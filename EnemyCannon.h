#ifndef ENEMYCANNON_H
#define ENEMYCANNON_H

#include "Entity.h"
#include "AttackQ.h"

class EnemyCannon : public Entity
{
	public:
		EnemyCannon();
		~EnemyCannon();
		
		virtual void init();
		virtual void init(int x, int y);
		virtual void init(int graphic, int x, int y, int w, int h);

		virtual void render(int offX = 0, int offY = 0);
		virtual void update();
		void checkRange(Entity* objective);
		C_Rectangle getAttackQ();
		int getAttackDamage(char c);

		void updateControls();
		
		bool isOfClass(std::string classType);
		std::string getClassName(){return "EnemyCannon";};

		//! Stores a pointer to the camera
		//! \param cam C_Rectangle of the camera
		void setCamera(C_Rectangle *cam);

	protected:


	private:
		C_Rectangle				*mpCamera;
		AttackQ					*mpAttackQ;
		int						mpViewRange;

};

#endif
