var annotated_dup =
[
    [ "C_Rectangle", "struct_c___rectangle.html", "struct_c___rectangle" ],
    [ "C_Triangle", "struct_c___triangle.html", "struct_c___triangle" ],
    [ "Entity", "class_entity.html", "class_entity" ],
    [ "MapManager", "class_map_manager.html", "class_map_manager" ],
    [ "ofApp", "classof_app.html", "classof_app" ],
    [ "Player", "class_player.html", "class_player" ],
    [ "Point", "struct_point.html", "struct_point" ],
    [ "RenderColor", "struct_render_color.html", "struct_render_color" ],
    [ "ResourceManager", "class_resource_manager.html", "class_resource_manager" ],
    [ "Scene", "class_scene.html", "class_scene" ],
    [ "SceneDirector", "class_scene_director.html", "class_scene_director" ],
    [ "SceneGame", "class_scene_game.html", "class_scene_game" ],
    [ "SceneMenu", "class_scene_menu.html", "class_scene_menu" ]
];