var hierarchy =
[
    [ "C_Rectangle", "struct_c___rectangle.html", null ],
    [ "C_Triangle", "struct_c___triangle.html", null ],
    [ "Entity", "class_entity.html", [
      [ "Player", "class_player.html", null ]
    ] ],
    [ "MapManager", "class_map_manager.html", null ],
    [ "ofBaseApp", null, [
      [ "ofApp", "classof_app.html", null ]
    ] ],
    [ "Point", "struct_point.html", null ],
    [ "RenderColor", "struct_render_color.html", null ],
    [ "ResourceManager", "class_resource_manager.html", null ],
    [ "Scene", "class_scene.html", [
      [ "SceneGame", "class_scene_game.html", null ],
      [ "SceneMenu", "class_scene_menu.html", null ]
    ] ],
    [ "SceneDirector", "class_scene_director.html", null ]
];