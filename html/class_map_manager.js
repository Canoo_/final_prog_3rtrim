var class_map_manager =
[
    [ "MapManager", "class_map_manager.html#a4a81e7c7db9ae561694db39f12d3807f", null ],
    [ "~MapManager", "class_map_manager.html#ac4024a4688cb432385228b584593b3a0", null ],
    [ "getClassName", "class_map_manager.html#a4153e160aa8896d02f01fb82f0a7b639", null ],
    [ "getCollision", "class_map_manager.html#af9b15f975c6b9cdf932a8fb4a0e986e5", null ],
    [ "getMapHeight", "class_map_manager.html#aa020b877274533426caaaa8cc155d1d9", null ],
    [ "getMapWidth", "class_map_manager.html#af3d51d43668f66fd5669e8a7ae1df98f", null ],
    [ "loadMap", "class_map_manager.html#a45fea054c375a221f2d5ff410322d556", null ],
    [ "renderMap", "class_map_manager.html#aca11591afc0e238daaa2dbf2d23cc96f", null ],
    [ "setObjectLayer", "class_map_manager.html#a169837d0eafd4f9fca9513228ed8dd98", null ],
    [ "setTileset", "class_map_manager.html#ad16d28e5d0e99f1cf9e7751b0800b3e3", null ],
    [ "mpCollisionLayer", "class_map_manager.html#a5713ab07c59f64ecc9e7f04e1e34a9a7", null ],
    [ "mpLayers", "class_map_manager.html#a52c5a658bd8a478d3fc2e38d4e1f6770", null ],
    [ "mpMapHeight", "class_map_manager.html#a51a91a50b60e5aaa25fea9359b4b0207", null ],
    [ "mpMapLayers", "class_map_manager.html#a155a7a268b175b6ae48b663de7592c97", null ],
    [ "mpMapLoaded", "class_map_manager.html#a4c4fc60a7301b29c48f27bf4af84d2de", null ],
    [ "mpMapWidth", "class_map_manager.html#af02c4373ae185e6f74a801886e2ae603", null ],
    [ "mpObjectLayer", "class_map_manager.html#a9c1237cbd47f2370e096fd3841d6f64c", null ],
    [ "mpTilesetH", "class_map_manager.html#ab5a4f67fed57610f3e086457ab2f0e3b", null ],
    [ "mpTilesetID", "class_map_manager.html#a1f6597ce77a51d9a7af127255f8e8800", null ],
    [ "mpTilesetRect", "class_map_manager.html#a047e4326759630eeb36d7c5f7174644c", null ],
    [ "mpTilesetW", "class_map_manager.html#ac79572ce52c7e3ac8a9803c2513b0b2d", null ]
];