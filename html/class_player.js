var class_player =
[
    [ "Player", "class_player.html#affe0cc3cb714f6deb4e62f0c0d3f1fd8", null ],
    [ "~Player", "class_player.html#a749d2c00e1fe0f5c2746f7505a58c062", null ],
    [ "getClassName", "class_player.html#a85d9336604ad741797758180898003ed", null ],
    [ "init", "class_player.html#a015ea21fa1e7273e47d48cb20d9b12e3", null ],
    [ "init", "class_player.html#a9aac0b24060caa8dec3e30d72c38f649", null ],
    [ "init", "class_player.html#a66bc8851bf782446a9d1cb510ef105fe", null ],
    [ "isOfClass", "class_player.html#ab0a4438a792cff19bc9b2cc0211cd90b", null ],
    [ "render", "class_player.html#afe3ff7815f9366b4c4fffa2cf6556ad5", null ],
    [ "update", "class_player.html#a82c3476f3e65a4e2ac6bcd040771bdd4", null ],
    [ "updateControls", "class_player.html#a3ee54ebb1f8aefecdce72f88cd62ce42", null ]
];