var class_scene_director =
[
    [ "SceneEnum", "class_scene_director.html#a62f26e903ae29c3ce71e519ee174149b", [
      [ "MAIN", "class_scene_director.html#a62f26e903ae29c3ce71e519ee174149ba9d0c52f7016b91a1d3afaa2d02ec2edb", null ],
      [ "LEVEL", "class_scene_director.html#a62f26e903ae29c3ce71e519ee174149ba29f29b7f8f2887e964abe76991752039", null ],
      [ "GAME_OVER", "class_scene_director.html#a62f26e903ae29c3ce71e519ee174149baa04a16feacf45218ed337280ae7744ca", null ],
      [ "SCENE_FIRST", "class_scene_director.html#a62f26e903ae29c3ce71e519ee174149bae39088438bfa92458ba73aeead75e591", null ],
      [ "SCENE_LAST", "class_scene_director.html#a62f26e903ae29c3ce71e519ee174149ba84bc05d7c0200ee5c21f196bc98814a1", null ]
    ] ],
    [ "SceneFlags", "class_scene_director.html#a40324819d46a7b97aca7415a4fe0cd00", [
      [ "FLAG1", "class_scene_director.html#a40324819d46a7b97aca7415a4fe0cd00a50f247f19de52fa365cf9c611cb4068c", null ],
      [ "FLAG2", "class_scene_director.html#a40324819d46a7b97aca7415a4fe0cd00af3eec84bdbaa506d61ba648892318a39", null ],
      [ "FLAG3", "class_scene_director.html#a40324819d46a7b97aca7415a4fe0cd00a9d895e575c042e9bdb1562cf8f5f29d7", null ],
      [ "FLAG_FIRST", "class_scene_director.html#a40324819d46a7b97aca7415a4fe0cd00afa77521d746d0f4566d905ccf4c012a2", null ],
      [ "FLAG_LAST", "class_scene_director.html#a40324819d46a7b97aca7415a4fe0cd00a4580ccef74bf039e78642fc5d02a1a64", null ]
    ] ],
    [ "SceneDirector", "class_scene_director.html#a69d70a75c13a0a611faebec612e02a4a", null ],
    [ "~SceneDirector", "class_scene_director.html#a1230e88aa3ce8769fadf1ba20caa597c", null ],
    [ "changeScene", "class_scene_director.html#af37f4dc556cf803947915d03d0e20c98", null ],
    [ "exitGame", "class_scene_director.html#ad1e75f2abca13acba145a6168a5fe860", null ],
    [ "getClassName", "class_scene_director.html#a2edd9133a91450f7ed60c4bf9302d56e", null ],
    [ "getCurrentScene", "class_scene_director.html#a4b7e17182832faeccf109e22ef5f4dae", null ],
    [ "getCurrSceneEnum", "class_scene_director.html#a073c6397b8f7ff2bc8823caa7a267947", null ],
    [ "getPrevSceneEnum", "class_scene_director.html#af6bcaf2f4722ba73f5986de9bece1710", null ],
    [ "getScene", "class_scene_director.html#ab8f0610192c393b09062c6b5a100d73c", null ],
    [ "goBack", "class_scene_director.html#a91ea10679f955bf5279868273f204f5c", null ],
    [ "init", "class_scene_director.html#a293e0ca9dadc384dac9f7306b454145f", null ],
    [ "setPreviousScene", "class_scene_director.html#a16fda6161377b8d9febd01ebfa885767", null ],
    [ "mCurrScene", "class_scene_director.html#a11d3f4e6c3e7589b9a4471b6efd64256", null ],
    [ "mFlags", "class_scene_director.html#a339544774022234b102ce8ec8252e47d", null ],
    [ "mPrevScene", "class_scene_director.html#a2476ee24e52e9444d7eb186a6efb699b", null ],
    [ "mVectorScenes", "class_scene_director.html#afb9e38782fc0d38e3aed262e1864d4a7", null ]
];