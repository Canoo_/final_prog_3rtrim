#include "AttackQ.h"
#include "singletons.h"

AttackQ::AttackQ() : Entity() {
	mpRect.x = 0;
	mpRect.y = 0;
	mpRect.w = 32;
	mpRect.h = 32;

	mpStats.mpMoveSpeed = 500;

	mpGraphicRect.w = 32;
	mpGraphicRect.h = 32;

	mpAttacking = false;
	mpAlive = true;
	mpGraphicImg = sResManager->getGraphicID("attack_q.png");
	mpRange = 200;
}

void AttackQ::init(int damage) {
	mpStats.mpAttackDamage = damage;
}

AttackQ::~AttackQ(){

}

void AttackQ::update(){
	Entity::update();
	
	if (mpAttacking) {
		int dx = abs(mpRect.x - mpInitialX);
		int dy = abs(mpRect.y - mpInitialY);
		if (sqrt(pow(dx, 2) + pow(dy, 2)) > mpRange) {
			mpAttacking = false;
			return;
		}
		
		mpRect.x += speed_x * (int)global_delta_time / 1000;
		mpRect.y += speed_y * (int)global_delta_time / 1000;

	}
	else {
		mpRect.x = 0;
		mpRect.y = 0;
	}
}
void AttackQ::render(int offX, int offY) {
	if (!mpAttacking) { return; }
	Entity::render(offX, offY);
}

bool AttackQ::isOfClass(std::string classType){
	if (classType == "Attack" ||
		classType == "Entity") {
		return true;
	}
	return false;
}

void AttackQ::setAttack(int x, int y) {
	if (mpAttacking) { return; }
	mpAttacking = true;
	mpRect.x = x - mpRect.w / 2;
	mpRect.y = y - mpRect.h / 2;
	mpInitialX = mpRect.x;
	mpInitialY = mpRect.y;
	int dist_x = (mpRect.x + mpRect.w / 2) - mpTargetX;
	int dist_y = (mpRect.y + mpRect.h / 2) - mpTargetY;
	int h = sqrt(pow(dist_x, 2) + pow(dist_y, 2));

	if (dist_x <= 0) {
		speed_x = (mpStats.mpMoveSpeed * abs(dist_x)) / h;
	}
	else {
		speed_x = -((mpStats.mpMoveSpeed * abs(dist_x)) / h);
	}

	if (dist_y <= 0) {
		speed_y = (mpStats.mpMoveSpeed * abs(dist_y)) / h;
	}
	else {
		speed_y = -((mpStats.mpMoveSpeed * abs(dist_y)) / h);
	}

	
	return;
}

void AttackQ::setTarget(int x, int y, Entity *entity) {
	if (mpAttacking) { return; }
	Entity::setTarget(x, y, entity);
}