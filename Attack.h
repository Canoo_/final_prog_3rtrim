#ifndef ATTACK_H
#define ATTACK_H

#include "Entity.h"

class Attack : public Entity
{
	public:
		Attack();
		void init(int damage);
		~Attack();

		void update();
		void render(int offX = 0, int offY = 0);
		void setAttack(int x, int y, bool attack);

		bool isOfClass(std::string classType);
		std::string getClassName(){return "Attack";};
		void setTarget(int x, int y, Entity *entity);

	protected:
	
	private:
		bool mpAttacking;
	
};

#endif
