#include "singletons.h"

//Include our classes
#include "GameOver.h"

GameOver::GameOver(): Scene(){ //Calls constructor in class Scene
}

GameOver::~GameOver(){
}

void GameOver::init(){
	Scene::init(); //Calls to the init method in class Scene
	mpGraphicID = sResManager->getGraphicID("gameover.png");
	mpGraphicRect.x = 0;
	mpGraphicRect.y = 0;
	mpGraphicRect.w = sResManager->getGraphicWidth(mpGraphicID);
	mpGraphicRect.h = sResManager->getGraphicHeight(mpGraphicID);
	mpTime = 0;
}

void GameOver::load(){
	Scene::load(); //Calls to the init method in class Scene
}

void GameOver::updateScene(){
	inputEvent();
	mpTime += global_delta_time;
	if (mpTime > 4000) { exit(0); }
}

void GameOver::drawScene(){
	ofSetColor(255, 0, 0);
	ofDrawRectangle(50, 50, 100, 100);
	imgRender(mpGraphicID, 200, 300, mpGraphicRect, 255);
}

//-------------------------------------------
//				   INPUT
//-------------------------------------------
void GameOver::inputEvent() {
	return;
}
