//Include our classes
#include "Player.h"
#include "singletons.h"


Player::Player() : Entity(){
	mpStats.mpMoveSpeed = 300;
	mpStats.mpAttackRange = 150;
	mpStats.mpAttackDamage = 1;
	mpStats.mpAttackSpeed = 1000;
	mpStats.mpAttackSpeedTimer = 1000;
	mpAttacks.resize(5);
	for (int i = 0; i < 5; i++) {
		mpAttacks[i] = new Attack();
		mpAttacks[i]->init(mpStats.mpAttackDamage);
	}
	mpAttackIndex = 0;
	mpAttackQ = new AttackQ();
	mpAttackQ->init(mpStats.mpAttackOneDamage);
	mpStats.mpAttackOneDamage = 1;
	mpStats.mpAttackOneCooldown = 3000;
	mpStats.mpAttackOneTimer = 3000;
	mpInfoGraphic = sResManager->getGraphicID("info.png");
	mpLifeID = sResManager->getGraphicID("life.png");
	mpManaID = sResManager->getGraphicID("mana.png");
	mpStats.mpLife = 60;
	mpStats.mpCurrentLife = 60;
}

Player::~Player(){
}

void Player::init(){
	Entity::init();
}
void Player::init(int x, int y){
	Entity::init(x, y);
}
void Player::init(int graphic, int x, int y, int w, int h){
	Entity::init(graphic, x, y, w, h);
	mpTargetX = x;
	mpTargetY = y;
}

void Player::render(int offX, int offY) {
	Entity::render(offX, offY);
	for (int i = 0; i < 5; i++) {
		mpAttacks[i]->render(offX, offY);
	}
	mpAttackQ->render(offX, offY);
	imgRender(mpInfoGraphic, 0, 0, { 0,0,SCREEN_WIDTH, SCREEN_HEIGHT });

	for (int i = 0; i <= mpStats.mpCurrentLife; i++) {
		imgRender(mpLifeID, 577 + 2 * i, 491, {0,0,2,20});
	}

	for (int i = 0; i <= mpStats.mpAttackOneTimer / 100; i++) {
		imgRender(mpManaID, 279 + 4 * i, 491, { 0,0,4,20 });
	}
}

void Player::update() {
	if (!mpAlive) { exit(0); }
	Entity::update();
	
	for (int i = 0; i < 5; i++) {
		mpAttacks[i]->update();
	}
	mpAttackQ->update();
}

void Player::updateControls() {
	checkKeys();
	
	mpDirection = NONE;
	mpStats.mpAttackSpeedTimer += global_delta_time;
	

	if (mpRect.x + mpRect.w / 2 + 5 > mpTargetX && mpRect.x + mpRect.w / 2 - 5 < mpTargetX) {
		mpRect.x = mpTargetX - mpRect.w / 2;
	}
	if (mpRect.y + mpRect.h / 2 + 5 > mpTargetY && mpRect.y + mpRect.h / 2 - 5 < mpTargetY) {
		mpRect.y = mpTargetY - mpRect.h / 2;
	}


	if (mpRect.x + mpRect.w / 2 < mpTargetX && mpRect.y + mpRect.h / 2 > mpTargetY) {		// UP_RIGHT
		mpDirection = UP_RIGHT;
	}
	else if (mpRect.y + mpRect.h / 2 > mpTargetY && mpRect.x + mpRect.w / 2 > mpTargetX) {	// UP_LEFT
		mpDirection = UP_LEFT;
	}
	else if (mpRect.y + mpRect.h / 2 < mpTargetY && mpRect.x + mpRect.w / 2 < mpTargetX) {	// DOWN_RIGHT	
		mpDirection = DOWN_RIGHT;
	}
	else if (mpRect.y + mpRect.h / 2 < mpTargetY && mpRect.x + mpRect.w / 2 > mpTargetX) {	// DOWN_LEFT
		mpDirection = DOWN_LEFT;
	}
	else if (mpRect.x + mpRect.w/2 < mpTargetX) {
		mpDirection = RIGHT;
	}
	else if (mpRect.x + mpRect.w / 2 > mpTargetX) {
		mpDirection = LEFT;
	}
	else if (mpRect.y + mpRect.h / 2 > mpTargetY) {
		mpDirection = UP;
	}
	else if (mpRect.y + mpRect.h / 2 < mpTargetY) {
		mpDirection = DOWN;
	}


	if (mpTargetEntity != NULL) {
		mpTargetX = mpTargetEntity->getX() + mpTargetEntity->getW() / 2;
		mpTargetY = mpTargetEntity->getY() + mpTargetEntity->getW() / 2;
		int	px = mpRect.x + mpRect.w / 2;
		int	py = mpRect.y + mpRect.h / 2;
		int xx = abs(px - mpTargetX);
		int yy = abs(py - mpTargetY);
		if (sqrt(xx*xx + yy*yy) < mpStats.mpAttackRange) {
			mpDirection = NONE;
			if (mpStats.mpAttackSpeedTimer >= mpStats.mpAttackSpeed) {
				mpStats.mpAttackSpeedTimer = 0;
				mpAttacks[mpAttackIndex]->setTarget(mpTargetX, mpTargetY, mpTargetEntity);
				mpAttacks[mpAttackIndex]->setAttack(mpRect.x + mpRect.w / 2, mpRect.y + mpRect.h / 2, true);
				mpAttackIndex++;
				if (mpAttackIndex >= 5) {
					mpAttackIndex = 0;
				}
			}
		}
	}
	

	return;
}

void Player::setCamera(C_Rectangle *cam) {
	mpCamera = cam;
	return;
}

bool Player::isOfClass(std::string classType){
	if(classType == "Player" || 
		classType == "Entity"){
		return true;
	}
	return false;
}

void Player::checkKeys() {
	if (mpStats.mpAttackOneTimer < mpStats.mpAttackOneCooldown) {
		mpStats.mpAttackOneTimer += global_delta_time;
	}

	if (key_down['q'] || key_down['Q']) {
		activateAttack('q');
		std::cout << "q pressed" << std::endl;
	}

	if (mouse.pressed[0]) {
		if (mpStats.mpAttackOneActivated) {
			mpAttackQ->setTarget(mouse.x + mpCamera->x, mouse.y + mpCamera->y, NULL);
			mpAttackQ->setAttack(mpRect.x + mpRect.w / 2, mpRect.y + mpRect.h / 2);
			activateAttack('n');
			mpStats.mpAttackOneTimer = 0;
		}
	}
}

void Player::activateAttack(char attack) {
	if (attack == 'q') {
		if (mpStats.mpAttackOneTimer < mpStats.mpAttackOneCooldown) { return; }
		mpStats.mpAttackOneActivated = true;
	}
	else if (attack == 'w') {
	
	}
	else if (attack == 'e') {

	}
	else if (attack == 'r') {

	}
	else if (attack == 'n') {
		mpStats.mpAttackOneActivated = false;
	}
}

C_Rectangle Player::getAttackQ() {
	return mpAttackQ->getRect();
}

int Player::getAttackDamage(char c) {
	if (c == 'Q' || c == 'q') {
		mpAttackQ->setXY(-32, -32);
		return mpStats.mpAttackOneDamage;
	}

	return 0;
}

void Player::dealDamage(int damage) {
	Entity::dealDamage(damage);
	if (!mpAlive) { exit(0); }
}