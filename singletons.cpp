#include "singletons.h"

ResourceManager*	sResManager;
SceneDirector*		sDirector;
GameState*			sGameState;
MapManager*			sMapManager;

void instanceSingletons(){
	sResManager	  = ResourceManager::getInstance();
	sDirector	  = SceneDirector::getInstance();
	sGameState	  = GameState::getInstance();
	sMapManager   = MapManager::getInstance();
}