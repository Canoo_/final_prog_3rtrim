#ifndef ENEMYCASTER_H
#define ENEMYCASTER_H

#include "Entity.h"
#include "Attack.h"

class EnemyCaster : public Entity
{
	public:
		EnemyCaster();
		~EnemyCaster();
		
		virtual void init();
		virtual void init(int x, int y);
		virtual void init(int graphic, int x, int y, int w, int h);

		virtual void render(int offX = 0, int offY = 0);
		virtual void update();
		void checkRange(Entity* objective);

		void updateControls();
		
		bool isOfClass(std::string classType);
		std::string getClassName(){return "EnemyCaster";};

		//! Stores a pointer to the camera
		//! \param cam C_Rectangle of the camera
		void setCamera(C_Rectangle *cam);

	protected:


	private:
		C_Rectangle				*mpCamera;
		std::vector <Attack*>	mpAttacks;
		int						mpAttackIndex;
		int						mpViewRange;

};

#endif
